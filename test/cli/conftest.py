from click.testing import CliRunner
from pytest import fixture


@fixture
def runner() -> CliRunner:
    runner: CliRunner = CliRunner()

    return runner
