import os
import shutil
from os.path import abspath
from pathlib import Path
from typing import List

import pytest
from click.testing import CliRunner, Result

from mosaik_multi_project.multi_project.library.get_log_message_module import \
    get_log_message
from mosaik_multi_project.multi_project.library.list_operations_module import \
    list_operations
from mosaik_multi_project.multi_project.cli.main import main
from mosaik_multi_project.multi_project.library.\
    get_command_line_arguments_module import get_command_line_arguments
from mosaik_multi_project.multi_project.library.load_project_aliases_module \
    import load_project_aliases
from mosaik_multi_project.multi_project.library.strings import \
    ALL_OPERATIONS_MAGIC_WORD, ALL_PROJECTS_MAGIC_WORD
from mosaik_multi_project.multi_project.util.paths import DATA_PATH


@pytest.mark.parametrize(
    "parallel",
    [
        True, False,
    ]
)
@pytest.mark.parametrize(
    "operations",
    [
        [
            list_operations()[0],  # Git clone has no dependencies
        ]
    ] + [
        [
            # venv create
            list_operations()[7],
        ]
    ] + [
        [ALL_OPERATIONS_MAGIC_WORD],
    ]
)
@pytest.mark.parametrize(
    "projects",
    [
        ['SimPy.IO_SemVer'],
    ]
)
@pytest.mark.parametrize(
    "configuration_file_paths",
    [
        [str(Path(DATA_PATH / 'config_mosaik_semver.json'))],
    ]
)
def test_main(
    runner: CliRunner,
    parallel: bool,
    operations: List[str],
    projects: List[str],
    configuration_file_paths: List[str],
):
    target = '../tmp'

    # Format mock data
    arguments = get_command_line_arguments(
        operations=operations,
        parallel=parallel,
        projects=projects,
        target=target,
        configurations=configuration_file_paths,
    )

    projects_path = Path(abspath(target))

    try:
        os.mkdir(path=projects_path)
    except FileExistsError:
        shutil.rmtree(path=projects_path)
        os.mkdir(path=projects_path)

    for project in projects:
        project_path = projects_path / project
        try:
            os.mkdir(path=project_path)
        except FileExistsError:
            shutil.rmtree(path=project_path)
            os.mkdir(path=project_path)

    # Test
    result = runner.invoke(
        main,
        arguments,
    )

    shutil.rmtree(path=projects_path)

    # If needed, Expand command line arguments.
    if projects == [ALL_PROJECTS_MAGIC_WORD]:
        projects = load_project_aliases(
            configuration_file_paths=configuration_file_paths,
        )

    if operations == [ALL_OPERATIONS_MAGIC_WORD]:
        operations = list_operations()

    # Assert
    expected_output_start = get_log_message(
        operation=tuple(operations),
        parallel=bool(parallel),
        project=tuple(projects),
    )

    expected_output_end = '[INFO] Done running operations on projects!\n'
    assert isinstance(result, Result)
    assert result.stdout.startswith(expected_output_start)
    assert result.stdout.endswith(expected_output_end)
    assert result.exception is None
    assert result.exc_info[1].code == SystemExit(0).code
    assert result.exit_code == 0
    assert result.output.startswith(expected_output_start)
    assert result.output.endswith(expected_output_end)
