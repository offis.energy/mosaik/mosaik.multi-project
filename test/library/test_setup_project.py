import os
import shutil
from os.path import abspath
from pathlib import Path

from mosaik_multi_project.multi_project.library.list_operations_module import \
    list_operations
from mosaik_multi_project.multi_project.library.setup_project import \
    setup_project


def test_setup_project():
    # Mock
    project = {
        "alias": "mosaik.EId",
        "repository": "https://gitlab.com/offis.energy/mosaik/mosaik.eid.git",
        "branch": "master",
        "project_path": None,
        "venv_python_name": "python3",
        "venv_requirements_txt_file_path": "requirements.d/venv.txt",
        "tox_environment_name": "py38",
        "container_name": None,
        "tox_requirements_txt_file_path":  "requirements.d/base.txt",
    }
    operations = list_operations()

    projects_path = Path(abspath('../tmp'))

    try:
        os.mkdir(path=projects_path)
    except FileExistsError:
        shutil.rmtree(path=projects_path)
        os.mkdir(path=projects_path)

    # Test
    setup_project(
        projects_path=projects_path,
        operations=operations,
        project=project,
    )

    shutil.rmtree(path=projects_path)

    # Assert
    assert True  # If we get here, we can consider the test successful.
