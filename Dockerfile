FROM python:3.9.2-slim-buster

# Copy our application to the image
COPY . /app
WORKDIR /app

# Setup the venv virtual environment
RUN \
    python3.9 -m venv venv && \
    venv/bin/python3.9 -m pip install --quiet --upgrade --no-cache-dir \
        -r requirements.d/venv.txt

# Setup the runtime virtual environment
RUN \
    venv/bin/python3.9 -m tox -e py39 --notest

# Run the application
ENV PYTHONPATH=.
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENTRYPOINT [".tox/py39/bin/python3.9", "./mosaik_multi_project/multi_project/cli/main.py"]
