import datetime

from setuptools import setup, find_namespace_packages

NAMESPACE = 'mosaik_multi_project'
PACKAGE = 'multi_project'
TIMESTAMP = str(datetime.datetime.now().replace(microsecond=0).isoformat()).\
    replace('-', '').replace('T', '').replace(':', '')

setup(
    author='Bengt Lüers',
    author_email='bengt.lueers@gmail.com',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU Lesser General Public License v2 '
        '(LGPLv2)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Scientific/Engineering',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    cmdclass={
    },
    description='Manage multiple mosaik projects with using common tooling.',
    entry_points={
    },
    include_package_data=True,
    install_requires=[
        'click>=7.0',
    ],
    long_description=(
        open('README.md').read()
    ),
    long_description_content_type='text/markdown',
    maintainer='Bengt Lüers',
    maintainer_email='bengt.lueers@gmail.com',
    name='mosaik' + '.' + 'Multi-Project',
    packages=find_namespace_packages(include=[NAMESPACE + '.*']),
    package_dir={'': '.'},
    setup_requires=[
    ],
    tests_require=[
    ],
    url='https://gitlab.com/offis.energy/mosaik/mosaik.multi-project',
    version='0.1.0' + 'rc' + TIMESTAMP,
    zip_safe=False,
)
